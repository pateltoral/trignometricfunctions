package com.se.lab7;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * This class tests TrignometricFunctions.java class using JUnit
 * @author GroupSE11
 *
 */
public class TrignometricFunctionsTest {

	double delta = 0.001;
	int degree1 = 30, degree2 = 45, degree3 = 60, degree4 = 90, degree5 = 120, degree6 = 135, degree7 = 150,
			degree8 = 180, degree9 = 270, degree10 = 360;

	double radian1 = Math.PI / 6, radian2 = Math.PI / 4, radian3 = Math.PI / 3, radian4 = Math.PI / 2,
			radian5 = (2 * Math.PI) / 3, radian6 = (3 * Math.PI) / 4, radian7 = (5 * Math.PI) / 6,
			radian8 = Math.sin(Math.PI), radian9 = (3 * Math.PI) / 2, radian10 = 2 * Math.PI;

	@Test
	public void sineTestAngle1() {
		assertEquals(Math.sin(radian1), TrignometricFunctions.sine(degree1), delta);
	}

	@Test
	public void sineTestAngle2() {
		assertEquals(Math.sin(radian2), TrignometricFunctions.sine(degree2), delta);
	}

	@Test
	public void sineTestAngle3() {
		assertEquals(Math.sin(radian3), TrignometricFunctions.sine(degree3), delta);
	}

	@Test
	public void sineTestAngle4() {
		assertEquals(Math.sin(radian4), TrignometricFunctions.sine(degree4), delta);
	}

	@Test
	public void sineTestAngle5() {
		assertEquals(Math.sin(radian5), TrignometricFunctions.sine(degree5), delta);
	}

	@Test
	public void sineTestAngle6() {
		assertEquals(Math.sin(radian6), TrignometricFunctions.sine(degree6), delta);
	}

	@Test
	public void sineTestAngle7() {
		assertEquals(Math.sin(radian7), TrignometricFunctions.sine(degree7), delta);
	}

	@Test
	public void sineTestAngle8() {
		assertEquals(Math.sin(radian8), TrignometricFunctions.sine(degree8), delta);
	}

	@Test
	public void sineTestAngle9() {
		assertEquals(Math.sin(radian9), TrignometricFunctions.sine(degree9), delta);
	}

	@Test
	public void sineTestAngle10() {
		assertEquals(Math.sin(radian10), TrignometricFunctions.sine(degree10), delta);
	}

	@Test
	public void cosineTestAngle1() {
		assertEquals(Math.cos(radian1), TrignometricFunctions.cosine(degree1), delta);
	}

	@Test
	public void cosineTestAngle2() {
		assertEquals(Math.cos(radian2), TrignometricFunctions.cosine(degree2), delta);
	}

	@Test
	public void cosineTestAngle3() {
		assertEquals(Math.cos(radian3), TrignometricFunctions.cosine(degree3), delta);
	}

	@Test
	public void cosineTestAngle4() {
		assertEquals(Math.cos(radian4), TrignometricFunctions.cosine(degree4), delta);
	}

	@Test
	public void cosineTestAngle5() {
		assertEquals(Math.cos(radian5), TrignometricFunctions.cosine(degree5), delta);
	}

	@Test
	public void cosineTestAngle6() {
		assertEquals(Math.cos(radian6), TrignometricFunctions.cosine(degree6), delta);
	}

	@Test
	public void cosineTestAngle7() {
		assertEquals(Math.cos(radian7), TrignometricFunctions.cosine(degree7), delta);
	}

	@Test
	public void cosineTestAngle8() {
		assertEquals(-1, TrignometricFunctions.cosine(degree8), delta);
	}

	@Test
	public void cosineTestAngle9() {
		assertEquals(Math.cos(radian9), TrignometricFunctions.cosine(degree9), delta);
	}

	@Test
	public void cosineTestAngle10() {
		assertEquals(Math.cos(radian10), TrignometricFunctions.cosine(degree10), delta);
	}

	@Test
	public void tanTestAngle1() {
		assertEquals(Math.tan(radian1), TrignometricFunctions.tan(degree1), delta);
	}

	@Test
	public void tanTestAngle2() {
		assertEquals(Math.tan(radian2), TrignometricFunctions.tan(degree2), delta);
	}

	@Test
	public void tanTestAngle3() {
		assertEquals(Math.tan(radian3), TrignometricFunctions.tan(degree3), delta);
	}

	@Test
	public void tanTestAngle5() {
		assertEquals(Math.tan(radian5), TrignometricFunctions.tan(degree5), delta);
	}

	@Test
	public void tanTestAngle6() {
		assertEquals(Math.tan(radian6), TrignometricFunctions.tan(degree6), delta);
	}

	@Test
	public void tanTestAngle7() {
		assertEquals(Math.tan(radian7), TrignometricFunctions.tan(degree7), delta);
	}

	@Test
	public void tanTestAngle8() {
		assertEquals(Math.tan(radian8), TrignometricFunctions.tan(degree8), delta);
	}

	@Test
	public void tanTestAngle10() {
		assertEquals(Math.tan(radian8), TrignometricFunctions.tan(degree10), delta);
	}

}
